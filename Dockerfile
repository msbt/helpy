FROM cloudron/base:0.10.0
MAINTAINER Authors name <support@cloudron.io>

ENV HELPY_VERSION=master \
    RAILS_ENV=production \
    HELPY_HOME=/app/code \
    PASSENGER_VERSION=5.2.3 \
    RUBY_VERSION=2.4.0

EXPOSE 3000
RUN sudo apt-get update && apt-get -y upgrade
RUN sudo apt-get install -y git-core imagemagick postgresql \
    postgresql-contrib libpq-dev curl build-essential zlib1g-dev \
    libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libcurl4-openssl-dev \
    libxml2-dev libxslt1-dev python-software-properties nodejs
RUN apt-get purge ruby2.3 -y
RUN apt-add-repository ppa:brightbox/ruby-ng && \
    apt-get update && \
    apt-get install ruby2.4 ruby2.4-dev -y

WORKDIR $HELPY_HOME
RUN git clone --branch $HELPY_VERSION --depth=1 https://github.com/helpyio/helpy.git .
RUN gem install rails --no-ri --no-rdoc -v 4.2.10
RUN gem install bundler passenger
RUN bundle install
RUN mkdir -p $HELPY_HOME/public/assets
RUN bundle exec rake assets:precompile
ADD database.yml /app/code/config/database.template.yml
ADD secrets.yml /app/code/config/secrets.template.yml
ADD settings.yml /app/code/config/settings.template.yml
RUN mv $HELPY_HOME/public $HELPY_HOME/public.template
RUN mv $HELPY_HOME/config $HELPY_HOME/config.template
RUN mv $HELPY_HOME/tmp $HELPY_HOME/tmp.template
RUN rm /app/code/log/production.log && ln -s /app/data/public $HELPY_HOME/public && ln -s /app/data/config $HELPY_HOME/config && \
    ln -s /app/data/bundler $HELPY_HOME/bundler && ln -s /run/production.log $HELPY_HOME/log/production.log && \
    ln -s /run/tmp $HELPY_HOME/tmp
RUN passenger-config build-native-support && \
    passenger-config install-agent && \
    passenger-install-nginx-module && \
    passenger-config install-standalone-runtime

RUN mv /var/lib/gems/${RUBY_VERSION}/gems/passenger-${PASSENGER_VERSION}/buildout/support-binaries /var/lib/gems/${RUBY_VERSION}/gems/passenger-${PASSENGER_VERSION}/buildout/support-binaries.orig && \
    ln -s /run/support-binaries /var/lib/gems/${RUBY_VERSION}/gems/passenger-${PASSENGER_VERSION}/buildout/support-binaries

ADD start.sh /app/code/start.sh
ADD nginx.site /etc/nginx/sites-enabled/app.site
RUN chmod +x /app/code/start.sh && chown -R cloudron /app

CMD [ "/app/code/start.sh" ]
