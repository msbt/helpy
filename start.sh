#!/bin/bash

set -eux
#export BUNDLE_PATH="/app/data"
if ! [ -f /app/data/.hy ]; then
 echo "=>First run<="
        mkdir -p /app/data/log /app/data/public /app/data/config /app/data/bundler /run/tmp
        touch /run/production.log
        cp -R /app/code/public.template/* /app/data/public
        cp -R /app/code/config.template/* /app/data/config
	cp -R /app/code/tmp.template/* /run/tmp
	echo "=>Set Secrets<="
	if ! [ -e /app/data/.salt ]; then
	  dd if=/dev/urandom bs=1 count=1024 2>/dev/null | sha1sum | awk '{ print $1 }' > /app/data/.salt
	fi
	SALT=$(cat /app/data/.salt)
	sed -e "s!repl1!${SALT}!" \
	config/secrets.template.yml > config/secrets.yml
        echo "=>Set DB<="
	sed -e "s!postgreshost!${POSTGRESQL_HOST}!" \
        -e "s!postgresdatabase!${POSTGRESQL_DATABASE}!" \
        -e "s!postgresusername!${POSTGRESQL_USERNAME}!" \
        -e "s!postgrespassword!${POSTGRESQL_PASSWORD}!" \
        config/database.template.yml > config/database.yml
	echo "=>Set Settings<="
        sed -e "s!repl1!${APP_ORIGIN}!" \
        -e "s!repl2!${MAIL_FROM}!" \
        -e "s!repl3!${MAIL_SMTP_USERNAME}!" \
        -e "s!repl4!${MAIL_SMTP_PASSWORD}!" \
        -e "s!repl5!${MAIL_SMTP_SERVER}!" \
        -e "s!repl6!${MAIL_SMTPS_PORT}!" \
        -e "s!repl7!${MAIL_DOMAIN}!" \
        config/settings.template.yml > config/settings.yml

        #Find a way to set the log location and chmod it
	echo "=>Populate DB<="
        bundle exec rake db:setup
        chown -R cloudron:cloudron /app/data /tmp /run
        touch /app/data/.hy
fi

echo "=>Start process<="
exec bundle exec unicorn -E production -c config/unicorn.rb &
echo "=>Start server<="
cp -rf /var/lib/gems/2.4.0/gems/passenger-5.2.3/buildout/support-binaries.orig /run/support-binaries
# ensure passenger does not write into /app/code
export TMPDIR=/run
passenger start --engine nginx --environment production --address 0.0.0.0 --port 3000 --pid-file /run/passenger.pid --log-file /run/passenger.log
